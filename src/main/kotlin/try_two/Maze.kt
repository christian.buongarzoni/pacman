package try_two

import java.awt.Point

const val DOT = "*"
const val EMPTY_FIELD = " "
const val PACMAN_RIGHT = "<"
const val PACMAN_DOWN = "Ʌ"
const val WALL = "■"

class Maze {
    private val grid = mutableListOf<MutableList<String>>()
    private var pacmanPosition = Point(1, 1)
    var eatenDots = 0
        private set

    init {
        grid.add(mutableListOf(DOT, DOT, DOT))
        grid.add(mutableListOf(DOT, DOT, DOT))
        grid.add(mutableListOf(DOT, DOT, WALL))
        setElementAt(pacmanPosition, PACMAN_RIGHT)
    }

    fun getElementAt(point: Point) = grid[point.y][point.x]

    fun tick() {
        val pacman = getElementAt(pacmanPosition)
        val nextPoint = getNextPoint(pacman)
        val nextElement = getElementAt(nextPoint)
        if (nextElement == WALL) {
            return
        }
        if (nextElement == DOT) {
            eatenDots++
        }
        setElementAt(pacmanPosition, EMPTY_FIELD)
        pacmanPosition = nextPoint
        setElementAt(pacmanPosition, pacman)
    }

    fun rotatePacman() {
        val pacman = getElementAt(pacmanPosition)

        if (pacman == PACMAN_DOWN) {
            setElementAt(pacmanPosition, PACMAN_RIGHT)
        } else {
            setElementAt(pacmanPosition, PACMAN_DOWN)
        }
    }

    private fun getNextPoint(pacman: String): Point {
        val addVector = if (pacman == PACMAN_RIGHT) Point(1, 0) else Point(0, 1)
        val nextPoint = Point(pacmanPosition.x + addVector.x, pacmanPosition.y + addVector.y)
        wrapIfOutboundWidth(nextPoint)
        wrapIfOutboundHeight(nextPoint)
        return nextPoint
    }

    private fun wrapIfOutboundWidth(nextPoint: Point) {
        if (nextPoint.x >= gridWidth()) nextPoint.translate((-nextPoint.x), 0)
    }

    private fun wrapIfOutboundHeight(nextPoint: Point) {
        if (nextPoint.y >= gridHeight()) nextPoint.translate(0, (-nextPoint.y))
    }

    private fun gridWidth() = grid[0].size
    private fun gridHeight() = grid.size

    private fun setElementAt(point: Point, element: String) {
        grid[point.y][point.x] = element
    }
}
