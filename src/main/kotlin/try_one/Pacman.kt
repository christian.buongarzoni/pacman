package try_one

class Pacman {
    var direction = Direction.RIGHT
       private set

    var dots = 0
        private set

    fun rotate() {
        direction = Direction.DOWN
    }

    fun eat() {
        dots++
    }
}
