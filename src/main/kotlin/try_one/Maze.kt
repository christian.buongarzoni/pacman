package try_one

import java.awt.Point

const val DOT = "."
const val GHOST = "ghost"

class Maze {
    private val grid = mutableListOf<MutableList<Any>>()
    private var pacmanPosition = Point(0, 0)

    var isGameOver = false
        private set
    val pacman = Pacman()

    init {
        fillGrid()
    }

    fun getElementAt(position: Point) = grid[position.y][position.x]

    fun tick() {
        movePacman(pacman.direction)
    }

    fun rotatePacman() {
        pacman.rotate()
    }

    private fun movePacman(direction: Direction) {
        when(direction) {
            Direction.RIGHT -> {
                val nextXField = if (pacmanPosition.x + 1 == width()) 0 else pacmanPosition.x + 1
                interactWithNextXElement(nextXField)
                moveInXDirection(nextXField)
            }
            Direction.DOWN -> {
                val nextYField = if (pacmanPosition.y + 1 == height()) 0 else pacmanPosition.y + 1
                interactWithNextYElement(nextYField)
                moveInYDirection(nextYField)
            }
        }
    }

    private fun interactWithNextXElement(nextXField: Int) {
        val elementInNewField = getElementAt(Point(nextXField, pacmanPosition.y))
        if(elementInNewField == DOT) {
            pacman.eat()
        }
    }

    private fun interactWithNextYElement(nextYField: Int) {
        val elementInNewField = getElementAt(Point(pacmanPosition.x, nextYField))
        if(elementInNewField == DOT) {
            pacman.eat()
        }

        if(elementInNewField == GHOST) {
            isGameOver = true
        }
    }

    private fun moveInXDirection(nextXField: Int) {
        setElementAt(pacmanPosition.y, nextXField, getElementAt(pacmanPosition))
        setElementAt(pacmanPosition.y, pacmanPosition.x, "")
        pacmanPosition.x = nextXField
    }

    private fun moveInYDirection(nextYField: Int) {
        setElementAt(nextYField, pacmanPosition.x, getElementAt(pacmanPosition))
        setElementAt(pacmanPosition.y, pacmanPosition.x, "")
        pacmanPosition.y = nextYField
    }

    private fun fillGrid() {
        val firstRow = mutableListOf(pacman, DOT)
        val secondRow = mutableListOf<Any>(GHOST, ".")
        grid.add(firstRow)
        grid.add(secondRow)
    }

    private fun setElementAt(row: Int, column: Int, element: Any) {
        grid[row][column] = element
    }

    private fun height() = grid.size
    private fun width() = grid[0].size
}
