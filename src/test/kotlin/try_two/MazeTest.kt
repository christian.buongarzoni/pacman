package try_two

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.awt.Point

internal class MazeTest {

    private lateinit var maze: Maze

    @BeforeEach
    fun setUp() {
        maze = Maze()
    }

    @Test
    fun `pacman is on a grid filled with dots and a wall`() {
        assertPointHaveElement(Point(0, 0), DOT)
        assertPointHaveElement(Point(1, 0), DOT)
        assertPointHaveElement(Point(2, 0), DOT)
        assertPointHaveElement(Point(0, 1), DOT)
        assertPointHaveElement(Point(1, 1), PACMAN_RIGHT)
        assertPointHaveElement(Point(2, 1), DOT)
        assertPointHaveElement(Point(0, 2), DOT)
        assertPointHaveElement(Point(1, 2), DOT)
        assertPointHaveElement(Point(2, 2), WALL)
    }

    @Test
    fun `after one rotation pacman looks down`() {
        maze.rotatePacman()
        assertPointHaveElement(Point(1, 1), PACMAN_DOWN)
    }

    @Test
    fun `after two rotations pacman back to starting position (right)`() {
        maze.rotatePacman()
        maze.rotatePacman()
        assertPointHaveElement(Point(1, 1), PACMAN_RIGHT)
    }

    @Test
    fun `pacman moves to point x=2, y=1 after one tick`() {
        maze.tick()
        assertPointHaveElement(Point(1, 1), EMPTY_FIELD)
        assertPointHaveElement(Point(2, 1), PACMAN_RIGHT)
    }

    @Test
    fun `pacman wraps around moving right`() {
        maze.tick()
        maze.tick()
        assertPointHaveElement(Point(0, 1), PACMAN_RIGHT)
    }

    @Test
    fun `pacman moves to point x=1, y=2 after rotate and a tick`() {
        maze.rotatePacman()
        maze.tick()
        assertPointHaveElement(Point(1, 1), EMPTY_FIELD)
        assertPointHaveElement(Point(1, 2), PACMAN_DOWN)
    }

    @Test
    fun `pacman wraps around moving down`() {
        maze.rotatePacman()
        maze.tick()
        maze.tick()
        assertPointHaveElement(Point(1, 0), PACMAN_DOWN)
    }

    @Test
    fun `pacman starts without eaten dots`() {
        assertEquals(0, maze.eatenDots)
    }

    @Test
    fun `pacman eat a dot when step into one while moving right`() {
        maze.tick()
        assertEquals(1, maze.eatenDots)
    }

    @Test
    fun `pacman eat a dot when step into one while moving down`() {
        maze.rotatePacman()
        maze.tick()
        assertEquals(1, maze.eatenDots)
    }

    @Test
    fun `pacman stops on wall while moving down`() {
        maze.tick()
        maze.rotatePacman()
        maze.tick()
        assertPointHaveElement(Point(2, 1), PACMAN_DOWN)
    }

    @Test
    fun `pacman stops on wall while moving right`() {
        maze.rotatePacman()
        maze.tick()
        maze.rotatePacman()
        maze.tick()
        assertPointHaveElement(Point(1, 2), PACMAN_RIGHT)
    }

    private fun assertPointHaveElement(point: Point, element: String) = assertEquals(element, maze.getElementAt(point))
}
