package try_one

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.awt.Point

internal class MazeTest {
    private lateinit var maze : Maze

    @BeforeEach
    fun setup() {
        maze = Maze()
    }

    @Test
    fun `pacman is in a maze with dots`() {
        assertHaveAPacman(Point(0, 0))
        assertHaveADot(Point(1, 1))
        assertHaveADot(Point(1, 0))
    }

    @Test
    fun `Pacman moves from 0,0 to 1,0 on tick`() {
        assertHaveAPacman(Point(0, 0))
        maze.tick()
        assertHaveAPacman(Point(1, 0))
    }

    @Test
    fun `Pacman wraps around maze`() {
        assertHaveAPacman(Point(0, 0))
        maze.tick()
        assertHaveAPacman(Point(1, 0))
        maze.tick()
        assertHaveAPacman(Point(0, 0))
    }

    @Test
    fun `Pacman eat dot when move to a tile with dot`() {
        assertEquals(0, maze.pacman.dots)
        maze.tick()
        assertEquals(1,  maze.pacman.dots)
    }

    @Test
    fun `Pacman die when collide with ghost`() {
        maze.rotatePacman()
        maze.tick()
        assertTrue(maze.isGameOver)
    }

    private fun assertHaveAPacman(position: Point) {
        assertTrue(maze.getElementAt(position) is Pacman)
    }

    private fun assertHaveADot(position: Point) {
        assertEquals(".", maze.getElementAt(position))
    }
}
