package try_one

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class PacmanTest {

    private lateinit var pacman : Pacman

    @BeforeEach
    fun setup() {
        pacman = Pacman()
    }

    @Test
    fun `pacman has an initial direction of right`() {
        assertEquals(Direction.RIGHT, pacman.direction)
    }

    @Test
    fun `pacman rotate`() {
        assertEquals(Direction.RIGHT, pacman.direction)
        pacman.rotate()
        assertEquals(Direction.DOWN, pacman.direction)
    }

    @Test
    fun `pacman eat dot`() {
        pacman.eat()
        assertEquals(1, pacman.dots)
    }
}
